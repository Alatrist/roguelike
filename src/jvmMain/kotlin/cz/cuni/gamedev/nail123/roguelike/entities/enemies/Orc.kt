package cz.cuni.gamedev.nail123.roguelike.entities.enemies

import cz.cuni.gamedev.nail123.roguelike.entities.attributes.HasSmell
import cz.cuni.gamedev.nail123.roguelike.entities.attributes.HasVision
import cz.cuni.gamedev.nail123.roguelike.entities.items.Sword
import cz.cuni.gamedev.nail123.roguelike.mechanics.Pathfinding
import cz.cuni.gamedev.nail123.roguelike.mechanics.Vision
import cz.cuni.gamedev.nail123.roguelike.mechanics.goBlindlyTowards
import cz.cuni.gamedev.nail123.roguelike.mechanics.goSmartlyTowards
import cz.cuni.gamedev.nail123.roguelike.tiles.GameTiles
import org.hexworks.zircon.api.data.Position3D
import kotlin.random.Random

class Orc: Enemy(GameTiles.ORC), HasVision {
    override val blocksMovement = true
    override val blocksVision = false

    override val cost = 12

    override val maxHitpoints = 20
    override var hitpoints = 10
    override var attack = 6
    override var defense = 2

    var hasSeenPlayer = false

    override val visionRadius = 7
    var attention = -3
    var target: Position3D = Position3D.unknown()

    override fun update() {
        val playerPosition = area.player.position
        val canSeePlayer = playerPosition in Vision.getVisiblePositionsFrom(area, position, visionRadius)

        if (canSeePlayer) hasSeenPlayer = true
        if (hasSeenPlayer) {
            if (attention <= -3)
                attention = 5
            if (attention > 0)
            {
                goSmartlyTowards(playerPosition)
            }
            else
            {
                hasSeenPlayer = false
            }
            attention--
        }
        else
        {
            if(attention == -3)
            {
                target = positions!![Random.nextInt(0, positions!!.size)]
                attention = -6
            }
            else
            {
                goSmartlyTowards(target)
                attention++
            }
        }
    }

    override fun die() {
        super.die()
        // Drop a sword
        this.block.entities.add(Sword(2))
    }
}