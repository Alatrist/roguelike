package cz.cuni.gamedev.nail123.roguelike.entities.enemies

import cz.cuni.gamedev.nail123.roguelike.entities.attributes.HasVision
import cz.cuni.gamedev.nail123.roguelike.entities.items.Sword
import cz.cuni.gamedev.nail123.roguelike.mechanics.Vision
import cz.cuni.gamedev.nail123.roguelike.mechanics.goCrab
import cz.cuni.gamedev.nail123.roguelike.mechanics.goSmartlyTowards
import cz.cuni.gamedev.nail123.roguelike.tiles.GameTiles
import org.hexworks.zircon.api.data.Position3D
import kotlin.random.Random

class Crab:Enemy(GameTiles.CRAB), HasVision {
    override val blocksMovement = true
    override val blocksVision = false


    override val maxHitpoints = 10
    override var hitpoints = 4
    override var attack = 4
    override var defense = 2

    override val visionRadius = 7
    var attention = 0
    var target: Position3D = Position3D.unknown()

    override val cost = 9

    override fun update() {
        val playerPosition = area.player.position
        val canSeePlayer = playerPosition in Vision.getVisiblePositionsFrom(area, position, visionRadius)
        if (canSeePlayer)
        {
            goCrab(playerPosition)
        }
        else
        {
            if (attention == 0)
            {

                target = positions!![Random.nextInt(0, positions!!.size)]
                attention = 5
            }
            goSmartlyTowards(target)
        }
        attention--
    }

    override fun die() {
        super.die()
        // Drop a sword
        this.block.entities.add(Sword(2))
    }
}