package cz.cuni.gamedev.nail123.roguelike.mechanics

import cz.cuni.gamedev.nail123.roguelike.entities.GameEntity
import cz.cuni.gamedev.nail123.roguelike.entities.enemies.*
import cz.cuni.gamedev.nail123.roguelike.entities.items.Item
import cz.cuni.gamedev.nail123.roguelike.entities.items.Meat
import cz.cuni.gamedev.nail123.roguelike.entities.items.Potion
import cz.cuni.gamedev.nail123.roguelike.entities.items.Sword
import cz.cuni.gamedev.nail123.roguelike.entities.objects.Chest
import cz.cuni.gamedev.nail123.roguelike.entities.objects.Vase
import kotlin.random.Random

object LootSystem {
    interface ItemDrop{
        fun getDrops():List<Item>
    }
    object NoDrop: ItemDrop{
        override fun getDrops() = listOf<Item>()
    }
    class SingleDrop(val instantiator: () -> Item): ItemDrop {
        override fun getDrops() = listOf(instantiator())
    }

    class TreasureClass(val numDrops: Int, val possibleDrops: List<Pair<Int, ItemDrop>>): ItemDrop {
        val totalProb = possibleDrops.map { it.first }.sum()

        override fun getDrops(): List<Item> {
            val drops = ArrayList<Item>()
            repeat(numDrops) {
                drops.addAll(pickDrop().getDrops())
            }
            return drops
        }

        private fun pickDrop(): ItemDrop {
            var randNumber = Random.Default.nextInt(totalProb)
            for (drop in possibleDrops) {
                randNumber -= drop.first
                if (randNumber < 0) return drop.second
            }
            // Never happens, but we need to place something here anyway
            return possibleDrops.last().second
        }
    }



    // Store rng for convenience
    val rng = Random.Default

    // Sword with power 2-4
    val basicSword = SingleDrop { Sword(rng.nextInt(3) + 2) }
    // Sword with power 5-6
    val rareSword = SingleDrop { Sword(rng.nextInt(2) + 5) }

    val snack = SingleDrop { Meat() }
    val potion = SingleDrop { Potion()}

    val basicTreasure = TreasureClass(1, listOf(
            1 to snack,
            1 to potion,
            1 to basicSword
    ))
    val rareTreasure = TreasureClass(3, listOf(
            1 to potion,
            1 to rareSword
    ))

    val treasureDrop = mapOf(
            Vase::class to basicTreasure,
            Chest::class to rareTreasure
    )

    val enemyDrops = mapOf(
            Rat::class to TreasureClass(1, listOf(
                    2 to NoDrop,
                    2 to snack,
                    1 to basicSword
            )),
            Orc::class to TreasureClass(1, listOf(
                    6 to NoDrop,
                    1 to basicSword,
                    1 to rareSword,
                    4 to snack,
                    2 to potion
            )),
            Snake::class to TreasureClass(1, listOf(
                    3 to NoDrop,
                    2 to snack,
                    1 to potion
            )),
            Crab::class to TreasureClass(1, listOf(
                    3 to NoDrop,
                    2 to snack,
                    1 to potion
            ))
    )



    fun onDeath(enemy: Enemy) {
        val drops = enemyDrops[enemy::class]?.getDrops() ?: return
        for (item in drops) {
            enemy.area[enemy.position]?.entities?.add(item)
        }
    }
    fun onCrash(furniture: GameEntity) {
        val drops = treasureDrop[furniture::class]?.getDrops() ?: return
        for (item in drops) {
            furniture.area[furniture.position]?.entities?.add(item)
        }
    }

}
