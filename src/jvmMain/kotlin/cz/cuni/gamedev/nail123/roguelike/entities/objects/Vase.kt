package cz.cuni.gamedev.nail123.roguelike.entities.objects

import cz.cuni.gamedev.nail123.roguelike.entities.GameEntity
import cz.cuni.gamedev.nail123.roguelike.entities.Player
import cz.cuni.gamedev.nail123.roguelike.entities.attributes.Interactable
import cz.cuni.gamedev.nail123.roguelike.entities.attributes.InteractionType
import cz.cuni.gamedev.nail123.roguelike.entities.attributes.interactionContext
import cz.cuni.gamedev.nail123.roguelike.entities.enemies.Snake
import cz.cuni.gamedev.nail123.roguelike.mechanics.LootSystem
import cz.cuni.gamedev.nail123.roguelike.tiles.GameTiles
import kotlin.random.Random

class Vase: Furniture(GameTiles.VASE), Interactable {
    override val blocksMovement = true
    override val blocksVision = true
    override val cost = 16

    override fun acceptInteractFrom(other: GameEntity, type: InteractionType) = interactionContext(other, type) {
        withEntity<Player>(InteractionType.BUMPED) {
            this@Vase.area?.removeEntity(this@Vase)
            LootSystem.onCrash(this@Vase)
            if(Random.nextFloat()<0.3)
                this@Vase.area[this@Vase.position]?.entities?.add(Snake())
        }
    }


}