package cz.cuni.gamedev.nail123.roguelike.entities.objects

import cz.cuni.gamedev.nail123.roguelike.entities.GameEntity
import org.hexworks.zircon.api.data.Tile

abstract class Furniture(tile: Tile): GameEntity(tile) {
    abstract val cost:Int
}
