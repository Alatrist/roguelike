package cz.cuni.gamedev.nail123.roguelike.entities.objects

import cz.cuni.gamedev.nail123.roguelike.entities.GameEntity
import cz.cuni.gamedev.nail123.roguelike.entities.attributes.HasCombatStats
import cz.cuni.gamedev.nail123.roguelike.entities.attributes.Interactable
import cz.cuni.gamedev.nail123.roguelike.entities.attributes.InteractionType
import cz.cuni.gamedev.nail123.roguelike.entities.attributes.interactionContext
import cz.cuni.gamedev.nail123.roguelike.tiles.GameTiles

class Trap: Furniture(GameTiles.TRAP), Interactable {
    override val blocksMovement = false
    override val blocksVision = false

    override val cost = 16

    override fun acceptInteractFrom(other: GameEntity, type: InteractionType) = interactionContext(other, type) {
        withEntity<HasCombatStats>(InteractionType.STEPPED_ON) {
            (other as HasCombatStats).takeDamage(2)
        }
    }

}