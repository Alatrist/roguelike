package cz.cuni.gamedev.nail123.roguelike.entities.objects

import cz.cuni.gamedev.nail123.roguelike.entities.GameEntity
import cz.cuni.gamedev.nail123.roguelike.entities.Player
import cz.cuni.gamedev.nail123.roguelike.entities.attributes.Interactable
import cz.cuni.gamedev.nail123.roguelike.entities.attributes.InteractionType
import cz.cuni.gamedev.nail123.roguelike.entities.attributes.interactionContext
import cz.cuni.gamedev.nail123.roguelike.mechanics.LootSystem
import cz.cuni.gamedev.nail123.roguelike.tiles.GameTiles


class Chest: Furniture(GameTiles.CHEST), Interactable {
    override val blocksMovement = true
    override val blocksVision = true

    override val cost = 48


    override fun acceptInteractFrom(other: GameEntity, type: InteractionType) = interactionContext(other, type) {
        withEntity<Player>(InteractionType.BUMPED) {
            LootSystem.onCrash(this@Chest)
            this@Chest.area.removeEntity(this@Chest)

        }
    }

}