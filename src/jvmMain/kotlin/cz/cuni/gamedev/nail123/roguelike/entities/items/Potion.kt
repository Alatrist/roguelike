package cz.cuni.gamedev.nail123.roguelike.entities.items

import cz.cuni.gamedev.nail123.roguelike.entities.GameEntity
import cz.cuni.gamedev.nail123.roguelike.entities.Player
import cz.cuni.gamedev.nail123.roguelike.entities.attributes.*
import cz.cuni.gamedev.nail123.roguelike.tiles.GameTiles
import kotlin.random.Random

class Potion: Item(GameTiles.POTION), Interactable {
    override val blocksMovement = false
    override val blocksVision = false
    override fun isEquipable(character: HasInventory): Inventory.EquipResult {
        return Inventory.EquipResult(false, "")
    }

    override fun onEquip(character: HasInventory) {
        TODO("Not yet implemented")
    }

    override fun onUnequip(character: HasInventory) {
        TODO("Not yet implemented")
    }

    override fun acceptInteractFrom(other: GameEntity, type: InteractionType) = interactionContext(other, type) {
        withEntity<Player>(InteractionType.STEPPED_ON) {
            val p = (other as Player)
            val rngVal = Random.nextInt(0, 3)
            if (rngVal == 0)
                p.hitpoints+=10
            else if (rngVal == 1)
                p.attack++
            else if (rngVal == 2)
                p.defense++
            else
                p.maxHitpoints += 10
            this@Potion.area?.removeEntity(this@Potion)
        }
    }
}