package cz.cuni.gamedev.nail123.roguelike.world.worlds

import cz.cuni.gamedev.nail123.Room
import cz.cuni.gamedev.nail123.roguelike.GameConfig
import cz.cuni.gamedev.nail123.roguelike.blocks.GameBlock
import cz.cuni.gamedev.nail123.roguelike.blocks.Wall
import cz.cuni.gamedev.nail123.roguelike.entities.enemies.*
import cz.cuni.gamedev.nail123.roguelike.entities.objects.*
import cz.cuni.gamedev.nail123.roguelike.entities.unplacable.FogOfWar
import cz.cuni.gamedev.nail123.roguelike.mechanics.Pathfinding
import cz.cuni.gamedev.nail123.roguelike.tiles.GameTiles
import cz.cuni.gamedev.nail123.roguelike.world.Area
import cz.cuni.gamedev.nail123.roguelike.world.builders.wavefunctioncollapse.WFCAreaBuilder
import org.hexworks.zircon.api.data.Position3D
import java.util.*
import kotlin.reflect.full.primaryConstructor

class WaveFunctionCollapsedWorld: DungeonWorld() {
    fun findRoom(rooms: MutableList<Room>, pos: Position3D): Room? {
        for (r in rooms) {
            if (r.contains(pos))
                return r
        }
        return null
    }


    fun isCorridor(area: WFCAreaBuilder, pos: Position3D): Boolean {
        return ((area[Position3D.create(pos.x + 1, pos.y, 0)] is Wall || area[Position3D.create(pos.x + 1, pos.y, 0)] == null)
                &&
                (area[Position3D.create(pos.x - 1, pos.y, 0)] is Wall || area[Position3D.create(pos.x - 1, pos.y, 0)] == null))
                ||
                ((area[Position3D.create(pos.x, pos.y + 1, 0)] is Wall || area[Position3D.create(pos.x, pos.y + 1, 0)] == null)
                        &&
                        (area[Position3D.create(pos.x, pos.y - 1, 0)] is Wall || area[Position3D.create(pos.x, pos.y - 1, 0)] == null))
    }


    fun connect(r: Room, rooms: MutableList<Room>, area: WFCAreaBuilder):Boolean
    {
        var target: Room? = null
        var min = Int.MAX_VALUE
        //take the nearest room
        for(i in rooms)
        {
            val dist = r.dist(i)
            if (dist< min)
            {
                min = dist
                target = i
            }
        }
        val rng = Random()
        if (target!=null)
        {
            val max_x_start = maxOf(r.min_x, target.min_x)
            val min_x_end = minOf(r.max_x, target.max_x)


            val max_y_start = maxOf(r.min_y, target.min_y)
            val min_y_end = minOf(r.max_y, target.max_y)

            // connect vertically
            if (max_x_start<= min_x_end)
            {

                val x = max_x_start + if (min_x_end != max_x_start) rng.nextInt(min_x_end - max_x_start) else 0
                for (y in min_y_end-1 until max_y_start+1)
                {
                    area[Position3D.create(x, y, 0)] = GameBlock(GameTiles.FLOOR)
                }
            }
            else if (max_y_start<= min_y_end)   //connect horizontally
            {

                val y = max_y_start + if (min_y_end != max_y_start) rng.nextInt(min_y_end - max_y_start) else 0
                for (x in min_x_end-1 until max_x_start+1) {
                    area[Position3D.create(x, y, 0)] = GameBlock(GameTiles.FLOOR)
                }
            }
            else    //cannot connect
                return false
            rooms.add(r)
            r.adjacent_rooms.add(target)
            target.adjacent_rooms.add(r)
            return true
        }
        return false
    }

    //go around the edge of the room and check open spaces
    fun detectExit(area: WFCAreaBuilder, room: Room)
    {
        var inCorridor1 = false
        var inCorridor2 = false
        for(y in room.min_y until room.max_y)
        {
            if(!(area[Position3D.create(room.min_x-1, y, 0)] is Wall || area[Position3D.create(room.min_x-1, y, 0)]==null))
            {
                if(!inCorridor1)
                {
                    room.exits.add(Pair(room.min_x-1, y))
                    inCorridor1 = true
                }
            }
            else
                inCorridor1=false

            if(!(area[Position3D.create(room.max_x, y, 0)] is Wall || area[Position3D.create(room.max_x, y, 0)] == null))
            {
                if(!inCorridor2)
                {
                    room.exits.add(Pair(room.max_x, y))
                    inCorridor2 = true
                }
            }
            else
                inCorridor2=false

        }
        for(x in room.min_x until room.max_x)
        {
            if(!(area[Position3D.create(x, room.min_y-1, 0)] is Wall || area[Position3D.create(x, room.min_y-1, 0)]==null))
            {
                if(!inCorridor1)
                {
                    room.exits.add(Pair(x, room.min_y-1))
                    inCorridor1 = true
                }
            }
            else
                inCorridor1 = false

            if(!(area[Position3D.create(x, room.max_y, 0)] is Wall || area[Position3D.create(x, room.max_y, 0)] == null))
            {
                if(!inCorridor2) {
                    room.exits.add(Pair(x, room.max_y))
                    inCorridor2 = true
                }
            }
            else
                inCorridor2=false
        }
    }

    //detect adjacent directly or via simple corridor
    fun adjacentDetection(start: Room, rooms:MutableList<Room>,  pos: Position3D): Room?
    {
        val end = findRoom(start.adjacent_rooms, pos)
        if (end==null)
        {
            val fullEnd = findRoom(rooms, pos)
            if (fullEnd!=null)
            {
                start.adjacent_rooms.add(fullEnd)
  
            }

        }
        else
        {
            start.adjacent_rooms.remove(end)
            if (end.adjacent_rooms.contains(start))
                end.adjacent_rooms.remove(start)
        }
        return end
    }

    //DFS cycle detection
    fun cycleDetection(start: Room, rooms:MutableList<Room>,  pos: Position3D): Room?
    {
        val deque: Deque<Room> = java.util.ArrayDeque<Room>()
        val end = findRoom(start.adjacent_rooms, pos)
        if (end == null)
            return null
        for(r in start.adjacent_rooms)
        {
            if (r != end)
                deque.addFirst(r);
        }
        start.visited = true
        end.visited = true
        var found = false

        while(deque.count()>0)
        {
            val el = deque.first
            deque.removeFirst()
            if (el == end)
            {
                found=true
                start.adjacent_rooms.remove(el)
                el.adjacent_rooms.remove(start)
                break;
            }
            else
            {
                if(!el.visited) {
                    el.visited = true
                    for (ad in el.adjacent_rooms) {
                        if (ad != start)
                            deque.addFirst(ad)
                    }
                }
            }

        }
        for(i in rooms)
            i.visited= false
        if (found)
            return end
        else
            return null
    }
    //creates graph of connected rooms
    fun allAccessible(rooms: MutableList<Room>, outRooms: MutableList<Room>)
    {
        val deque: Deque<Room> = java.util.ArrayDeque<Room>()
        var max: Room? = null
        var maxVal = Int.MIN_VALUE
        for(r in rooms)
        {
            if (r.adjacent_rooms.count()>maxVal)
            {
                maxVal = r.adjacent_rooms.count()
                max = r
            }
        }
        deque.addFirst(max)
        //DFS
        while (deque.count()>0)
        {
            val current = deque.first
            deque.removeFirst()
            if (!current.visited)
            {
                outRooms.add(current)
                current.visited=true
                for(r in current.adjacent_rooms)
                    deque.addFirst(r)
            }
        }
        for(r in rooms)
            r.visited = false
    }

    fun removeCyrcles(rooms: MutableList<Room>, area: WFCAreaBuilder, condition: ( Room, MutableList<Room>,  Position3D)-> Room?)
    {
        for(r in rooms)
        {
            for(e in r.exits)
            {
                var pos = Position3D.create(e.first, e.second,0)
                //not removed yet
                if (!(area[pos] is Wall || area[pos]==null))
                {
                    var x_dif = if (r.contains(Position3D.create(pos.x, r.min_y, 0))) 0 else 1
                    var y_dif = if (r.contains(Position3D.create(r.min_x, pos.y, 0))) 0 else 1

                    if (pos.x < r.min_x)
                        x_dif = -x_dif
                    if (pos.y < r.min_y)
                        y_dif = - y_dif

                    while(area[pos] != null)
                    {
                        if (isCorridor(area, pos))
                            pos = Position3D.create(pos.x + x_dif, pos.y + y_dif, 0)
                        else
                        {
                            var end = condition(r, rooms, pos)
                            if (end != null)    //second visit
                            {
                                x_dif = -x_dif
                                y_dif = -y_dif

                                pos = Position3D.create(pos.x + x_dif, pos.y + y_dif, 0)
                                //fill the corridor
                                while(! r.contains(pos))
                                {
                                    if (isCorridor(area, pos))
                                        area[pos] = Wall()//Wall()
                                       /* if (condition == ::cycleDetection)
                                            area[pos] = GameBlock(baseTile = GameTiles.SWORD)//Wall()
                                        else
                                            area[pos] = GameBlock(baseTile = GameTiles.RAT)*/
                                    pos = Position3D.create(pos.x + x_dif, pos.y + y_dif, 0)
                                }
                            }

                            break;
                        }
                    }

                }
            }
        }
    }


    /*fun fillCorridor(start: Room, startPos: Position3D, end: Room, area: WFCAreaBuilder)
    {
        var pos = startPos
        var x_dif = if (start.contains(Position3D.create(pos.x, start.min_y, 0))) 0 else -1
        var y_dif = if (start.contains(Position3D.create(start.min_x, pos.y, 0))) 0 else -1
        if (x_dif ==y_dif)
            x_dif == 0
        if (pos.x < start.min_x)
            x_dif = -x_dif
        if (pos.y < start.min_y)
            y_dif = - y_dif

        pos = Position3D.create(pos.x + x_dif, pos.y + y_dif, 0)
        //fill the corridor
        while(! r.contains(pos))
        {
            if (isCorridor(area, pos))
                area[pos] = GameBlock(baseTile = GameTiles.SWORD)//Wall()
            pos = Position3D.create(pos.x + x_dif, pos.y + y_dif, 0)
        }
    }*/

    //detect rectangular rooms
    fun detectRoom(rooms: MutableList<Room>, pos: Position3D, area: WFCAreaBuilder):Int
    {
        val room = findRoom(rooms, pos)
        //new space
        if (room == null)
        {
            //wall or corridor
            if((area[pos]is Wall || area[pos] == null) || isCorridor(area, pos))
                return pos.y+1
            //new room
            else
            {
                var max_x = area.width
                var max_y = area.height

                for(i in pos.x until area.width)
                {
                    if (area[Position3D.create(i, pos.y, 0)] is Wall || area[Position3D.create(i, pos.y, 0)]==null)
                    {
                        max_x = i
                        break;
                    }
                    for(j in pos.y until max_y)
                    {
                        val testPos = Position3D.create(i, j, 0)
                        if ((area[testPos] is Wall || area[testPos]==null) || isCorridor(area, pos)) {
                            max_y = minOf(max_y, j)
                            break;
                        }
                    }
                }
                val newRoom = Room(pos.x, pos.y, max_x, max_y)

                detectExit(area, newRoom)
                rooms.add(newRoom)
                return  newRoom.max_y
            }
        }
        else
        {
            return room.max_y
        }
    }

    fun fillRoom(room: Room, area: WFCAreaBuilder, allEnemies:MutableList<Enemy>)
    {
        var budget = room.budget()
        val furniture = mutableListOf<Furniture>(Chest(), Vase(), Trap())
        val enemies = mutableListOf<Enemy>(Rat(), Orc(), Crab(), Snake())

        var furnitureCost = 1
        var enemyCost = 1

        while(furniture.size>0 || enemies.size>0)
        {
            if (furniture.size>0 && kotlin.random.Random.nextFloat() > furnitureCost/(furnitureCost+enemyCost))
            {
                val pos = room.getEmpty(area, 0.8f)
                val r = kotlin.random.Random.nextInt(furniture.size)
                if (furniture[r].cost<budget)
                {
                    area[pos]?.entities?.add(furniture[r]::class.primaryConstructor?.call()!!)
                    budget -= furniture[r].cost
                    furnitureCost+=furniture[r].cost
                }
                else
                    furniture.removeAt(r)
            }
            else
            {
                val r = kotlin.random.Random.nextInt(enemies.size)
                val pos = room.getEmpty(area, 0f)
                if (enemies[r].cost<budget)
                {
                    val en = enemies[r]::class.primaryConstructor?.call()!!
                    allEnemies.add(en)
                    area[pos]?.entities?.add(en)
                    budget -= enemies[r].cost
                    enemyCost+= enemies[r].cost
                }
                else
                    enemies.removeAt(r)
            }


        }
    }


    override fun buildLevel(floor: Int): Area {
        val area = WFCAreaBuilder(GameConfig.AREA_SIZE).create()

        var x=0
        var y=0
        val rooms = mutableListOf<Room>()
        var currentRoom: Room? = null

        while(x < area.width-1)
        {
            while(y < area.height-1)
            {
                y = detectRoom(rooms, Position3D.create(x, y,0), area)
            }
            x++
            y=0
        }
        removeCyrcles(rooms, area, ::adjacentDetection)
        val graph: MutableList<Room> = mutableListOf()
        allAccessible(rooms, graph)
        val single: MutableList<Room> = mutableListOf()
        for(r in rooms)
        {
            if(!graph.contains(r))
                single.add(r)
        }

        var fails = 0
        var to_remove = mutableListOf<Room>()
        while(single.count()>0)
        {
            to_remove = mutableListOf<Room>()
            for(i in single.shuffled())
            {
                if(connect(i,graph, area))
                {
                    to_remove.add(i)
                    break
                }
            }
            if (to_remove.count()>0)
                to_remove.forEach{single.remove(it)}
            else
            {
                if (fails > 10)
                    break;
                else
                    fails++;

            }
        }
        removeCyrcles(rooms, area, ::cycleDetection)

        var dist = Pathfinding.floodFill(Position3D.create(rooms[0].min_x, rooms[0].min_y, 0), area)
        

        area.addAtEmptyPosition(
            area.player,
            Position3D.create(0, 0, 0),
            GameConfig.VISIBLE_SIZE
        )

        area.addEntity(FogOfWar(), Position3D.unknown())

        // Add stairs up
        if (floor > 0) area.addEntity(Stairs(false), area.player.position)


        val allEnemies = mutableListOf<Enemy>()

        for(r in rooms)
            fillRoom(r, area, allEnemies)

        val allPos = Pathfinding.floodFill(Position3D.create(rooms[0].min_x, rooms[0].min_y, 0), area).keys.toList()

        for(e in allEnemies)
            e.positions = allPos


        // Add stairs down
        val floodFill = Pathfinding.floodFill(area.player.position, area)
        val maxDistance = floodFill.values.max()!!
        val staircasePosition = floodFill.filter { it.value > maxDistance / 2 }.keys.random()
        area.addEntity(Stairs(), staircasePosition)

        return area.build()
    }
}