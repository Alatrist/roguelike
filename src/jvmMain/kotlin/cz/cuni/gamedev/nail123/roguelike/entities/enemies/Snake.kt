package cz.cuni.gamedev.nail123.roguelike.entities.enemies

import cz.cuni.gamedev.nail123.roguelike.entities.attributes.HasSmell
import cz.cuni.gamedev.nail123.roguelike.mechanics.Pathfinding
import cz.cuni.gamedev.nail123.roguelike.mechanics.goBlindlyTowards
import cz.cuni.gamedev.nail123.roguelike.tiles.GameTiles
import kotlin.random.Random

class Snake: Enemy(GameTiles.SNAKE), HasSmell {
    override val blocksMovement=true
    override val blocksVision=false
    override val smellingRadius=5

    override val maxHitpoints=10
    override var hitpoints=6
    override var attack=4
    override var defense=0

    override val cost=12

    override fun update() {
        if (Pathfinding.chebyshev(position, area.player.position) <= smellingRadius) {
            goBlindlyTowards(area.player.position)
        }
        else
            goBlindlyTowards(positions!![Random.nextInt(0, positions!!.size)])
    }


}