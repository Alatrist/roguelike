package cz.cuni.gamedev.nail123.roguelike.entities.items

import cz.cuni.gamedev.nail123.roguelike.entities.GameEntity
import cz.cuni.gamedev.nail123.roguelike.entities.Player
import cz.cuni.gamedev.nail123.roguelike.entities.attributes.*
import cz.cuni.gamedev.nail123.roguelike.tiles.GameTiles

class Meat: Item(GameTiles.MEAT), Interactable {
    override val blocksMovement = false
    override val blocksVision = false

    override fun isEquipable(character: HasInventory): Inventory.EquipResult {
        return Inventory.EquipResult(false, "")
    }

    override fun onEquip(character: HasInventory) {
        TODO("Not yet implemented")
    }

    override fun onUnequip(character: HasInventory) {
        TODO("Not yet implemented")
    }

    override fun acceptInteractFrom(other: GameEntity, type: InteractionType) = interactionContext(other, type){
        withEntity<Player>(InteractionType.STEPPED_ON) {
            (other as Player).hitpoints +=5
            this@Meat.area?.removeEntity(this@Meat)
        }
    }
}