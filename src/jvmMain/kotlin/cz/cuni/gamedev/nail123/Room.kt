package cz.cuni.gamedev.nail123

import cz.cuni.gamedev.nail123.roguelike.world.builders.wavefunctioncollapse.WFCAreaBuilder
import org.hexworks.zircon.api.data.Position3D
import java.lang.Math.sqrt
import kotlin.random.Random

class Room {
    constructor(min_x: Int, min_y: Int, max_x: Int, max_y: Int)
    {
        this.min_x = min_x
        this.min_y = min_y
        this.max_x = max_x
        this.max_y = max_y
    }
    fun contains(pos: Position3D): Boolean
    {
        return pos.x>=min_x && pos.y >= min_y
                && pos.x < max_x && pos.y < max_y
    }

    fun budget():Int
    {
        return (max_x - min_x)*(max_y - min_y)
    }

    fun getEmpty(area: WFCAreaBuilder, edgeProb:Float): Position3D
    {
        // prefer edges
        if (Random.nextFloat()<edgeProb)
        {
            for(i in 0..5)
            {
                var x = Random.nextInt(min_x, max_x)
                var pos = Position3D.create(x, min_y, 0)
                if (area[pos]!!.entities.size==0)
                    return pos
                pos = Position3D.create(x, max_y-1, 0)
                if (area[pos]!!.entities.size==0)
                    return pos
            }
            for(i in 0..5)
            {
                var y = Random.nextInt(min_y, max_y)
                var pos = Position3D.create(min_x, y, 0)
                if (area[pos]!!.entities.size==0)
                    return pos
                pos = Position3D.create(max_x-1, 1, 0)
                if (area[pos]!!.entities.size==0)
                    return pos
            }
        }

        //try random point
        var x = Random.nextInt(min_x, max_x)
        var y = Random.nextInt(min_y, max_y)

        var pos = Position3D.create(x, y ,0)

        if (area[pos]!!.entities.size==0)
            return pos


        val xUp = x+1 until max_x
        val xDown = x-1 downTo min_x
        val yUp = y+1 until max_y
        val yDown = y-1 downTo min_y

        var xChangeSelected:Iterable<Int> = 0..1
        var xChangeOther:Iterable<Int> = 0..1
        var yChangeSelected:Iterable<Int> = 0..1
        var yChangeOther:Iterable<Int> = 0..1

        if (Random.nextBoolean())
        {
            xChangeSelected = xUp
            xChangeOther = xDown
        }
        else
        {
            xChangeSelected = xDown
            xChangeOther = xUp
        }
        if (Random.nextBoolean())
        {
            yChangeSelected = yUp
            yChangeOther = yDown
        }
        else
        {
            yChangeSelected = yDown
            yChangeOther = yUp
        }


        for (i in xChangeSelected)
            for(j in yChangeSelected)
            {
                pos = Position3D.create(i, j, 0)
                if (area[pos]!!.entities.size==0)
                    return pos
            }
        for (i in xChangeOther)
            for(j in yChangeOther)
            {
                pos = Position3D.create(i, j, 0)
                if (area[pos]!!.entities.size==0)
                    return pos
            }
        return Position3D.unknown()
    }

    //minimal distance in vertical or horizontal direction
    fun dist(r:Room):Int
    {

        val max_x_start = maxOf(r.min_x, min_x)
        val min_x_end = minOf(r.max_x, max_x)

        val min_x_start = minOf(r.min_x, min_x)


        val max_y_start = maxOf(r.min_y, min_y)
        val min_y_end = minOf(r.max_y, max_y)

        val min_y_start = minOf(r.min_y, min_y)


        var min = Int.MAX_VALUE

        if (max_x_start<= min_x_end)
        {
            min = max_y_start-min_y_start
        }
        if (max_y_start<= min_y_end)
        {
            min = minOf(min, max_x_start-min_x_start)
        }
        return min
    }


    val max_x :Int
    val max_y :Int

    val min_x: Int
    val min_y: Int

    var visited: Boolean = false

    val adjacent_rooms: MutableList<Room> = mutableListOf()
    val exits: MutableList<Pair<Int, Int>> = mutableListOf()


}